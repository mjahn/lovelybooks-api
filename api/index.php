<?php
header('Access-Control-Allow-Origin: http://localhost:4000');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Request-Method: POST');
date_default_timezone_set('Europe/Berlin');

// respond to preflights
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	exit;
}

include_once(__DIR__ . '/../vendor/autoload.php');

function startSession($sid = null) {
	if ($sid !== null) {
		session_id($sid);
	}
	session_start();
}

function login($request, $response, $args) {
	startSession();
	session_regenerate_id();
	$client = new \Lovelybooks\Client();
	$data = $request->getParsedBody();

	return $response->withJson([
		'data' => [
			'user' => $client->login($data['email'], $data['password'], (isset($data['remember']) ? true : false)),
		],
		'sessionid' => session_id()
	], 200);
}

function logout($request, $response, $args) {
	startSession();
	session_destroy();
	return $response->withJson([
		'status' => 'ok',
		'data' => [
			'user' => [
				'loggedIn' => false,
				'name' => ''
			]
		]
	], 200);
}

function getLibraries($request, $response, $args) {
	startSession($args['session']);
	$client = new \Lovelybooks\Client();

	return $response->withJson([
		'status' => 'ok',
		'data' => [
			'libraries' => $client->getLibraries(),
		],
		'sessionid' => session_id()
	], 200);
}

function getMessagePage($request, $response, $args) {
	startSession($args['session']);
	$client = new \Lovelybooks\Client();

	return $response->withJson([
		'status' => 'ok',
		'data' => [
			'messages' => $client->getMessagePage($args['folder'], 20, $args['page'])
		],
		'sessionid' => session_id()
	], 200);
}

function getLibraryPage($request, $response, $args) {
	startSession($args['session']);
	$client = new \Lovelybooks\Client();

	$data = explode('/', $request->getAttribute('params'));
	$page = array_pop($data);

	return $response->withJson([
		'status' => 'ok',
		'data' => $client->getLibraryPage(join('/', $data), $page),
		'sessionid' => session_id()
	], 200);
}

function getTagPage($request, $response, $args) {
	startSession($args['session']);
	$client = new \Lovelybooks\Client();

	return $response->withJson([
		'status' => 'ok',
		'data' => $client->getTagPage($args['tag'], $args['page']),
		'sessionid' => session_id()
	], 200);
}

function checkUser($request, $response, $args) {
	startSession($args['session']);
	$client = new \Lovelybooks\Client();

	return $response->withJson([
		'status' => 'ok',
		'data' => [
			'user' => $client->getUserStatus()
		],
		'sessionid' => session_id()
	], 200);
}

function getBook($request, $response, $args) {
	startSession($args['session']);
	$client = new \Lovelybooks\Client();

	return $response->withJson([
		'status' => 'ok',
		'data' => [
			'book' => $client->getBook($args['author'], $args['title'])
		],
		'sessionid' => session_id()
	], 200);
}

function getBookStatus($request, $response, $args) {
	startSession($args['session']);
	$client = new \Lovelybooks\Client();

	return $response->withJson([
		'status' => 'ok',
		'data' => [
			'bookstatus' => $client->getBookStatus($args['status'])
		],
		'sessionid' => session_id()
	], 200);
}

function getUserDetails($request, $response, $args) {
	startSession($args['session']);
	$client = new \Lovelybooks\Client();

	return $response->withJson([
		'status' => 'ok',
		'data' => $client->getUserDetails($args['name']),
		'sessionid' => session_id()
	], 200);
}

function getAuthorDetails($request, $response, $args) {
	startSession($args['session']);
	$client = new \Lovelybooks\Client();

	return $response->withJson([
		'status' => 'ok',
		'data' => $client->getAuthorDetails($args['name']),
		'sessionid' => session_id()
	], 200);
}

function getPublisherDetails($request, $response, $args) {
	startSession($args['session']);
	$client = new \Lovelybooks\Client();

	return $response->withJson([
		'status' => 'ok',
		'data' => [
			'user' => $client->getPublisherDetails($args['name'])
		],
		'sessionid' => session_id()
	], 200);
}

function getGroups($request, $response, $args) {
	startSession($args['session']);
	$client = new \Lovelybooks\Client();

	return $response->withJson([
		'status' => 'ok',
		'data' => [
			'groups' => $client->getGroups()
		],
		'sessionid' => session_id()
	], 200);
}

function getGroup($request, $response, $args) {
	startSession($args['session']);
	$client = new \Lovelybooks\Client();

	return $response->withJson([
		'status' => 'ok',
		'data' => $client->getGroup($args['id'], $args['name']),
		'sessionid' => session_id()
	], 200);
}

function getRaffles($request, $response, $args) {
	startSession($args['session']);
	$client = new \Lovelybooks\Client();

	return $response->withJson([
		'status' => 'ok',
		'data' => $client->getRaffles(),
		'sessionid' => session_id()
	], 200);
}

function getForum($request, $response, $args) {
	startSession($args['session']);
	$client = new \Lovelybooks\Client();

	return $response->withJson([
		'status' => 'ok',
		'data' => $client->getForum('/' . $request->getAttribute('params')),
		'sessionid' => session_id()
	], 200);
}

function getThread($request, $response, $args) {
	startSession($args['session']);
	$client = new \Lovelybooks\Client();

	$url = $request->getAttribute('params');
	$url = str_replace('/tag/', '/?tag=', $url);

	return $response->withJson([
		'status' => 'ok',
		'data' => $client->getThread('/' . $url),
		'sessionid' => session_id()
	], 200);
}

function createMessage($request, $response, $args) {
	startSession($args['session']);
	$client = new \Lovelybooks\Client();
	$data = $request->getParsedBody();

	return $response->withJson([
		'status' => 'ok',
		'data' => [
			'message' => $client->createMessage($data['receipients'], $data['subject'], $data['message'])
		],
		'sessionid' => session_id()
	], 200);
}


function updateBookStatus($request, $response, $args) {
	startSession($args['session']);
	$client = new \Lovelybooks\Client();
	$data = $request->getParsedBody();

	return $response->withJson([
		'status' => 'ok',
		'data' => [
			'bookstatus' => $client->updateBookStatus($args['book'], $data['page'], $data['comment'], $data['status'])
		],
		'sessionid' => session_id()
	], 200);
}

$c = new \Slim\Container(['settings' => [ 'displayErrorDetails' => true ]]);
$c['errorHandler'] = function($c) {
	return function($request, $response, $exception) use ($c) {
		return $c['response']->withJson([
			'status' => 'failed',
			'error' => $exception->getMessage(),
			'sessionid' => session_id()
		], min(500, max(200, $exception->getCode())));
	};
};

$app = new \Slim\App($c);
$app->post('/login', 'login');
$app->get('/{session}/logout', 'logout');
$app->get('/{session}/libraries', 'getLibraries');
$app->get('/{session}/library[/{params:.*}]', 'getLibraryPage');
$app->get('/{session}/tag/{tag}/{page}', 'getTagPage');
$app->get('/{session}/book/status/{status}', 'getBookStatus');
$app->post('/{session}/book/status/{book}', 'updateBookStatus');
$app->get('/{session}/book/{author}/{title}', 'getBook');
$app->get('/{session}/messages/{folder}/{page}', 'getMessagePage');
$app->post('/{session}/messages', 'createMessage');
$app->get('/{session}/user', 'checkUser');
$app->get('/{session}/raffles', 'getRaffles');
$app->get('/{session}/groups', 'getGroups');
$app->get('/{session}/group/{id}/{name}', 'getGroup');
$app->get('/{session}/user/{name}', 'getUserDetails');
$app->get('/{session}/author/{name}', 'getAuthorDetails');
$app->get('/{session}/publisher/{name}', 'getPublisherDetails');
$app->get('/{session}/forum/{params:.*}', 'getForum');
$app->get('/{session}/thread/{params:.*}', 'getThread');
$app->run();
