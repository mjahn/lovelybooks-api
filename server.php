<?php
error_reporting(E_ALL);
/**
 * This is a router-script for running directly with builtin PHP-webserver
 */

$sPath = $_SERVER['REQUEST_URI'];
if(strpos($sPath, '/api') === 0) {
	$_SERVER['REQUEST_URI'] = str_replace('/api', '', $sPath);
	$_SERVER['PATH_INFO'] = str_replace('/api', '', $sPath);
	include_once(dirname(__FILE__) .'/api/index.php');
} else {
	return false;
}